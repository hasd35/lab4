#include <iostream>
#include <vector>
#include <map>
#include "enrutador.h"
#include "funciones.h"

using namespace std;


int main(){
	
	if(true){
	Red miRed("Leida");
	//Red miRed=cargarRedArchivo("red.txt");
	int opt=0;
	int optRout=0;
	
	do{
		string nomTemp="";
		string nombre;
		string routerOrigen,routerDestino;
		opt = opciones();
	    switch(opt){
	    	case 1:
	    		cout<<"\n\n------------Agregar------------"<<endl;
	    		cout <<"Ingrese nombre del enrutador: "<<endl;
	    		cin >> nomTemp;
	    		miRed.setEnrutador(crearEnrutador(nomTemp));
	    		
	    	break;
	    	case 2:
	    		cout<<"\n\n------------Eliminar-----------"<<endl;
	    		if(miRed.validarRed()){
		    		cout <<"Cual enrutador desea eliminar?";
		    		cin>>nomTemp;
		    		miRed.deleteEnrutador(nomTemp);
	    		}
	    		
	    	break;
	    	case 3:
	    		cout<<"\n\n-------------Actualizar-------------"<<endl;
	    		if(miRed.validarRed()){
	    			cout <<"Cual enrutador desea actualizar?";
	    			cin>>nomTemp;
					miRed.updateEnrutador(nomTemp);
				}
	    		
	    	break;	
	    	case 4:
	    		cout<<"Mostrar conexiones"<<endl;
	    		optRout = opcionesEnrutador();
	    		
	    		if(optRout == 1){
    				miRed.showConexionesEnrutadores();
				}else if(optRout == 2){
					string routName="";
	    			cout << "\tCual enrutador desea ver? ";
	    			cin >> routName;
			    	miRed.showConexionesEnrutador(routName); 
				}else{
					cout << "Ha ingresado una opcion incorrecta"<<endl;
				}
	    		
	    	break;
	    	case 5:
	    		cout<<"Mostrar red"<<endl;
	    		miRed.showListEnrutadores();
	    		miRed.mostrarTablaEnrutamiento();
	    		//c.test("C");
	    		//d.test();
	    		
	    	break;
	    	case 6:
	    		cout<<"ingrese el nombre del archivo"<<endl;
	    		cin>>nombre;
	    		miRed = cargarRedArchivo(nombre);
	    		miRed.calcularTabla();
	    		
	    	break;
	    	case 7:
	    		if(miRed.validarRed()){
		    		cout<<"Consultar costo desde un router <origen> a uno <destino>(Directamente conectados)\n"<<endl;
		    		cout<<"Ingrese el Router origen: ";
		    		cin>>routerOrigen;
		    		cout<<"Ingrese el Router Destino: ";
		    		cin>>routerDestino;
		    		miRed.calcularCostoSrcDst(routerOrigen,routerDestino);
	    		}
	    		
	    	break;
	    	case 8:
	    		if(miRed.validarRed()){
		    		cout<<"Consultar costo desde un router <origen> a uno <destino>\n"<<endl;
		    		cout<<"Ingrese el Router origen: ";
		    		cin>>routerOrigen;
		    		cout<<"Ingrese el Router Destino: ";
		    		cin>>routerDestino;
		    		miRed.calcularCostoMejorCamino(routerOrigen,routerDestino);
	    		}
	    		
	    	break;
	    	case 9:
	    			srand( time( NULL ) );
		    		cout<<"Cargar redes desde archivo\n"<<endl;
		    		cout<<"ingrese el nombre del archivo"<<endl;
	    			int numAleatorio = rand()%10+1;
	    			ostringstream convert; 
	    			convert << numAleatorio;
	    			nombre = "red"+convert.str()+".txt";
	    			cout<<"Se va a cargar la red =>"<< nombre<<endl;
	    			miRed = cargarRedArchivo(nombre);
	    		
	    		
	    	break;
	    	
		}
		
    }while(opt!=10);
	}else{
		/*vector<int> numero[3];

		for(int i=0;i<3;i++){
		    for(int j=0;j<3;j++){
		        int valor;
		        cin>>valor;
		        numero[i].push_back(valor);
		    }
		}
		
		for(int i=0;i<3;i++){
		    for(int j=0;j<3;j++){
		        cout<< numero[i].at(j);
		    }
		    cout << endl;
		}
		*/
		//Red miRed=cargarRedArchivo("red.txt");
		
		

	
		
		
	}
    
    
    cout <<"Ha finalizado la ejecucion"<<endl;
    return 0;
}

