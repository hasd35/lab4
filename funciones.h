#ifndef FUNCIONES_H
#include <iostream>
#include "enrutador.h"
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <time.h>
#define FUNCIONES_H

using namespace std;

int opciones();
int opcionesEnrutador();
enrutador crearEnrutador(string nombre);
Red cargarRedArchivo(string nombre);

#endif
