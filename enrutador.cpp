#include "enrutador.h"

int** tabla;


enrutador::enrutador(string name){
    nombre=name;
}

void enrutador::mostrarConexiones(){
	for(map<string,int>::iterator it = conexiones.begin(); it!=conexiones.end();it++){
        cout<<nombre<<"+"<<it->first<<"="<<it->second<<endl;
    }
	
}
map<string,int>* enrutador::getConexiones(){
	return &conexiones;
}

void enrutador::test(){
    for(map<string,int>::iterator it = conexiones.begin(); it!=conexiones.end();it++){
        cout<<"Desde el enrutador "<<nombre<<", la ruta al enrutador "<<it->first<<" cuesta "<<it->second<<endl;
    }
}

void enrutador::costoConexionCon(string a){
	for(map<string,int>::iterator it = conexiones.begin(); it!=conexiones.end();it++){
		if(it->first==a){
			cout<<"Desde el enrutador "<<nombre<<", la ruta al enrutador "<<it->first<<" cuesta "<<it->second<<endl;
		}
    }		
}

int enrutador::costoConexion(string a){
	for(map<string,int>::iterator it = conexiones.begin(); it!=conexiones.end();it++){
		if(it->first==a){
			//cout<<"Desde el enrutador "<<nombre<<", la ruta al enrutador "<<it->first<<" cuesta "<<it->second<<endl;
			return it->second;
		}
    }		
}

void enrutador::conectar(string A, int B){
      conexiones.insert( pair<string,int>(A,B));
}

void enrutador::desconectar(string A){
      conexiones.erase(A);
}

string enrutador::getEnrutadorNombre(){
	return nombre;
} 

bool operator== ( const enrutador &en1, const enrutador &en2) 
{
        return en1.nombre == en2.nombre;
}


Red::Red(string nombre){
	this->nombre = nombre;
	vector<enrutador> enrutadores;
}

bool Red::validarRed(){
	if(enrutadores.size()==0){
		cout <<endl<<"Esta red aun no tiene enrutadores"<<endl<<endl;
		return false;
	}
	return true;
}

void Red::setEnrutador(enrutador a){
	enrutadores.push_back(a);

}

void Red::actualizarEnrutador(string routOrig, string RoutDest, int cost){
	for(unsigned i=0;i<enrutadores.size();i++){
			enrutador *temp = &enrutadores.at(i);
			if(temp->getEnrutadorNombre()==routOrig){
				temp->conectar(RoutDest,cost);
			}
	}

	
}

void Red::deleteEnrutador(string a){
	if(validarRed() && existeEnrutador(a)){
		for(unsigned i=0;i<enrutadores.size();i++){
			enrutador *temp = &enrutadores.at(i);
			map<string,int>::iterator it;
			map<string,int> *conexiones = temp->getConexiones();
			
			if(temp->getEnrutadorNombre()==a){
				enrutadores.erase(enrutadores.begin()+i);
			}
			
			for (it=conexiones->begin(); it!=conexiones->end(); ++it){
				if(it->first==a){
					cout << it->first << "=>" <<it->second <<endl;
	    			temp->desconectar(a);
				}
    		}
			
		}
		//cout<<"El enrutador " <<a<<" no existe en "<<nombre<<endl;
	}
}
void Red::updateEnrutador(string a){
	string nomCon;
	int costo;
	if(validarRed()){
		for(unsigned i=0;i<enrutadores.size();i++){
			enrutador *temp = &enrutadores.at(i);
			if(temp->getEnrutadorNombre()==a){
				cout << "Ingrese el router que desea conectar: ";
				cin >> nomCon;
				cout << "Ingrese el costo desde "<<temp->getEnrutadorNombre()<<" hasta "<<nomCon<<": ";
				cin >> costo;
				temp->conectar(nomCon,costo);
				cout<<"Se ha actualizado correctamente el enrutador " <<a<<endl;
				return;
			}
		}
		cout<<"El enrutador " <<a<<" no existe en "<<nombre<<endl;
	}
	
}


void Red::showListEnrutadores(){
	if(validarRed()){
		cout <<endl<<"Entutadores de "<<nombre<<endl;
		for(int i=0;i<enrutadores.size();i++){
			cout <<(i+1)<<". Enrutador "<< enrutadores.at(i).getEnrutadorNombre()<<endl;
		}
	}
	
}
void Red::showConexionesEnrutadores(){
	if(validarRed()){
		for(int i=0;i<enrutadores.size();i++){
			enrutador temp=enrutadores.at(i);
			temp.test();
		}
	}
	
}
void Red::showConexionesEnrutador(string a){
	if(validarRed()){
		for(int i=0;i<enrutadores.size();i++){
			enrutador temp=enrutadores.at(i);
			if(temp.getEnrutadorNombre()==a){
				temp.mostrarConexiones();
				return;
			}		
		}
		cout<<"El enrutador " <<a<<" no existe en "<<nombre<<endl;
	}
	
}

bool Red::existeEnrutador(enrutador a){
	for(unsigned i=0;i<enrutadores.size();i++){
		enrutador temp = enrutadores.at(i);
		if(temp.getEnrutadorNombre()==a.getEnrutadorNombre()){
			return true;
		}
	}
	return false;	
}
bool Red::existeEnrutador(string a){
	for(unsigned i=0;i<enrutadores.size();i++){
		enrutador temp = enrutadores.at(i);
		if(temp.getEnrutadorNombre()==a){
			return true;
		}
	}
	return false;	
}

void Red::calcularCostoSrcDst(string src,string dst){
	if(validarRed()){
	
		int count=0;
		enrutador routSrc = enrutador(src);
		vector<enrutador>::iterator it;
		it = find(enrutadores.begin(), enrutadores.end(), routSrc);
		if (it != enrutadores.end()){
			routSrc = (*it);	
			//cout<<"\nDesde el enrutador "<<src<<", la ruta al enrutador "<<dst<<" cuesta "<<routSrc.costoConexion(dst)<<endl<<endl;
			map<string,int>::iterator itCon;
			map<string,int> *conexiones = routSrc.getConexiones();
			
			for (itCon=conexiones->begin(); itCon!=conexiones->end(); ++itCon){
				if(itCon->first==dst){
					cout <<"El costo desde "<<src<<" hasta "<<itCon->first << " es " <<itCon->second <<endl;
					return;	
				}else{
					count++;
				}
		    }
		}
		if(count>0){
			cout <<"No se encuentra no estan directamente conectados"<<endl;
		}
	}
}

void Red::calcularTabla(){
	vector<string> nomEnrutadores;

	tabla = new int*[enrutadores.size()];
	for(int i = 0; i < enrutadores.size(); ++i) {
    	tabla[i] = new int[enrutadores.size()];
	}
	for(int i = 0; i < enrutadores.size(); ++i) {
		for(int j = 0; j < enrutadores.size(); ++j) {
    	tabla[i][j]=0;
    	}
	}
	
	enrutador temp("");
	if(validarRed()){
		
		for(unsigned i=0;i<enrutadores.size();i++){
			nomEnrutadores.push_back(enrutadores.at(i).getEnrutadorNombre());
		}
		
		for(unsigned i=0;i<enrutadores.size();i++){
			
			temp =enrutadores.at(i);
			map<string,int>::iterator itCon;
			map<string,int> *conexiones = temp.getConexiones();
			 
			int posFil=find(nomEnrutadores.begin(),nomEnrutadores.end(),temp.getEnrutadorNombre())-nomEnrutadores.begin();			
			for (itCon=conexiones->begin(); itCon!=conexiones->end(); ++itCon){
				int posCol=find(nomEnrutadores.begin(),nomEnrutadores.end(),itCon->first)-nomEnrutadores.begin();
				int costo = itCon->second;
				tabla[posFil][posCol]=(costo);
			}
		}
		
		cout<<"\t";
		for(int i=0;i<enrutadores.size();i++)cout<<nomEnrutadores.at(i)<<"\t";
		cout<<endl;
		for(int i=0;i<enrutadores.size();i++){
			cout<<nomEnrutadores.at(i)<<"\t";
		    for(int j=0;j<enrutadores.size();j++){
		        cout<< tabla[i][j]<<"\t";
		    }
		    cout << endl;
		}
		//tablaEnrutamiento = tabla;
		
			
	}
}

void Red::mostrarTablaEnrutamiento(){
	
	vector<string> nomEnrutadores;
	for(unsigned i=0;i<enrutadores.size();i++) nomEnrutadores.push_back(enrutadores.at(i).getEnrutadorNombre());
	cout<<"\t";
		for(int i=0;i<enrutadores.size();i++)cout<<nomEnrutadores.at(i)<<"\t";
		cout<<endl;
		for(int i=0;i<enrutadores.size();i++){
			cout<<nomEnrutadores.at(i)<<"\t";
		    for(int j=0;j<enrutadores.size();j++){
		        cout<< tabla[i][j]<<"\t";
		    }
		    cout << endl;
		}
}


void Red::calcularCostoMejorCamino(string src,string dst){
	vector<string> nomEnrutadores;
	for(unsigned i=0;i<enrutadores.size();i++)nomEnrutadores.push_back(enrutadores.at(i).getEnrutadorNombre());
	int posFil=find(nomEnrutadores.begin(),nomEnrutadores.end(),src)-nomEnrutadores.begin();
	int posCol=find(nomEnrutadores.begin(),nomEnrutadores.end(),dst)-nomEnrutadores.begin();
	cout<<"El costo de "<<src<< " a "<< dst <<" = " << tabla[posFil][posCol]<<endl;	
	
}

enrutador Red::getEnrutador(string name){
	enrutador encontrado = enrutador(name);
	vector<enrutador>::iterator it;
	it = find(enrutadores.begin(), enrutadores.end(), encontrado);
	if (it != enrutadores.end()){
		encontrado = *it;
	}
	return encontrado;
}

