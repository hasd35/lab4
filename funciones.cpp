#include "funciones.h"


int opciones(){
	int opt=0;
	do{
		cout<<"Ingrese una opcion"<<endl;
		cout<<"1. Agregar enrutador"<<endl;
		cout<<"2. Eliminar enrutador"<<endl;
		cout<<"3. Actualizar tabla de enrutamiento"<<endl;
		cout<<"4. Mostrar conexiones"<<endl;
		cout<<"5. Mostrar red"<<endl;
		cout<<"6. Cargar red desde archivo"<<endl;
		cout<<"7. Consultar costo (Origen-Destino) sin saltos"<<endl;
		cout<<"8. Consultar costo (Origen-Destino) mejor ruta"<<endl;
		cout<<"9. Cargar redes desde archivo"<<endl;
		cout<<"10. Salir"<<endl;

		cin >>opt;
	}while(opt <= 0 || opt > 10 );
	
	return opt;
}

enrutador crearEnrutador(string nombre){
	enrutador *temp = new enrutador(nombre);
	int con=0;
	string nomTemp;
	int cost=0;
	cout<<"Cuantas conexiones tiene "<<nombre<<endl;
	cin >> con;
	if(con>0){
		for(int i=0;i<con;i++){
			cout <<"Ingrese el nombre de la conexion "<<(i+1)<<endl;
			cin >> nomTemp;
			cout <<"Ingrese el costo de "<<nomTemp <<" desde "<<nombre<<endl;
			cin >> cost;
			temp->conectar(nomTemp,cost);
		}
	}
	
	return *temp;
}

Red cargarRedArchivo(string nombre){
    
	Red *redLeida = new Red("Leida");
	fstream archivo;
	archivo.open(nombre.c_str(),ios::in);
	string linea;
	string texto="";
	int k=0;
	if(archivo.is_open()){
		while(getline(archivo,linea)){
			string temp1="",temp2="";
			string costo="";
			int cost;
			
			cout << linea<<"\t-\t";//<routOri>+<routDest>=costo
			temp1=linea.substr(0, linea.find("+"));
			temp2=linea.substr(linea.find("+")+1,linea.find("=")-2);
			costo = linea.substr(linea.find("=")+1,linea.size());
			
			stringstream geek(costo);
			geek>>cost;
			
			enrutador *router1 = new enrutador(temp1);
			router1->conectar(temp2,cost);
			router1->test();
			if(redLeida->existeEnrutador(temp1)){
				redLeida->actualizarEnrutador(temp1,temp2,cost);
			}else{
				redLeida->setEnrutador(*router1);
			}
			
		}
		archivo.close();
	}else{
		cout<<"No fue posible leer el archivo"<<endl;
	}
	//redLeida->calcularTabla();
	return *redLeida;
}

int opcionesEnrutador(){
	int opt=0;
	do{
		cout<<"\tIngrese una opcion"<<endl;
		cout<<"\t1. Mostrar todos"<<endl;
		cout<<"\t2. Mostrar un enrutador especifico"<<endl;
		cout<<"\t";
		cin >>opt;
	}while(opt <= 0 || opt > 2 );
	return opt;
}



	
